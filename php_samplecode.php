<?php

namespace ClubeFashion;

use \ClubeFashion\Models\Campanha;
use \ClubeFashion\Models\Produto;
use \ClubeFashion\Tree;

class ProductList
{
    private $db;
    // bool user access type
    private $previewMode;
    // arr  current category / position in tree
    private $categorySlugArray;
    private $childrenCatIDs;
    // arr  campaigns selected
    private $campaigns;
    private $campaignIDs;
    // filters
    private $order = array('nome' => 'ASC');
    private $priceRange = array('min' => -1, 'max' => -1);
    private $sizes = [];
    private $brandIDs;
    private $orderByCatIDs;
    private $tree;


    public function __construct($previewMode = false)
    {
        $this->previewMode = $previewMode;
        $this->db = new \ClubeFashion\DB();
    }

    public function setCampaigns($internalCodes = array())
    {
        if (empty($internalCodes)) {
            return;
        }

        $result = Campanha::whereIn('internal_code', $internalCodes)
                          ->select('id', 'nome', 'internal_code')
                          ->get();

        $this->campaigns = $result->toArray();

        foreach ($this->campaigns as $item) {
            $this->campaignIDs[] = $item['id'];
        }
    }

    // Function to set current category of campaign for filtering purposes
    public function setCategory($categorySlugArray, $toOrder = false)
    {

        // Store category slug array because it is a good thing to keep in hand
        if (!$toOrder) {
            $this->categorySlugArray = isset($categorySlugArray[0]) && !empty($categorySlugArray[0]) ? $categorySlugArray : array('montra', '', '', '');
        } else {
            $this->categorySlugArrayToOrder = isset($categorySlugArray[0]) && !empty($categorySlugArray[0]) ? $categorySlugArray : array('montra', '', '', '');
        }

        // Instantiate Tree
        $tree = $this->getTree();
        // Ask tree for ID corresponding to our category array and all of its children
        $childrenCatIDs = $tree->getCatIDsFromSlugArray($tree->data, $categorySlugArray, 0);

        if (!$toOrder) {
            $this->childrenCatIDs = $childrenCatIDs;
        } else {
            $this->orderByCatIDs = $childrenCatIDs;
        }
    }

    public function setCategoryByIDs($childrenCatIDs)
    {
        /*
        $this->childrenCatIDs = $childrenCatIDs;
        return;
        */
        $tree = $this->getTree();
        $allChildren = $tree->getChildrenCategoriesRecursive($childrenCatIDs, true);
        $this->childrenCatIDs = $allChildren;
    }

    // Function that loads order desired for product listing
    public function setOrder($order)
    {
        return $this->setMultipleOrder([$order]);
    }

    public function setMultipleOrder($orders)
    {
        $orderArr = [];

        foreach ($orders as $order) {
            switch ($order) {
                case 'relevancia':
                    $orderArr['produtos.ordem'] = 'ASC'; break;
                case 'alfabetica-crescente':
                    $orderArr['produtos.nome'] = 'ASC'; break;
                case 'alfabetica-decrescente':
                    $orderArr['produtos.nome'] = 'DESC'; break;
                case 'preco-crescente':
                    $orderArr['preco_cf'] = 'ASC'; break;
                case 'preco-decrescente':
                    $orderArr['preco_cf'] = 'DESC'; break;
            }
        }

        $this->order = $orderArr;
    }

    // Function that loads sizes array we wnat to filter products with
    public function setSizes($sizes)
    {
        if (!empty($sizes) && count($sizes > 0)) {
            $this->sizes = $sizes;
        }
    }

    // Function that loads price range we want to filter products with
    public function setPriceRange($min, $max)
    {
        if (!empty($min)) {
            $this->priceRange['min'] = $min;
        }
        if (!empty($max)) {
            $this->priceRange['max'] = $max;
        }
    }

    // Function that loads brand id we want to filter products with
    public function setBrands($brandIDs)
    {
        $this->brandIDs = $brandIDs;
    }

    // Function that returns filtered products
    public function get($page = 0, $from_start = false)
    {
        $productsQuery = $this->getProductsQuery();

        /*
        $pagesize = 30;
        $offset = $page * $pagesize;

        if ($from_start) {
            $productsQuery->limit($offset+$pagesize);
        } else {
            $productsQuery->limit($pagesize)
                          ->offset($offset);
        }
         */

        $products = $productsQuery->get();

        $products->each(function ($item, $key) {
            // Calculate lowest price and corresponding discount
            $productObject = new Product();
            $productObject->loadWithData($item);
            $prices = $productObject->calcPrices();

            $item->discount = $prices['discount'];
            $item->preco_cf = $prices['precoCF'];
            $item->preco_pvp = $prices['precoPVP'];

            // Convert image update date to unix timestamp
            $item->imagens->each(function ($imagem, $key) use ($item) {
                $imagem->updated_at = strtotime($imagem->updated_at);
                $imagem->src = '/campanhas/'.$item->campaign_internal_code.'/produtos/rev-'.$imagem->updated_at.'/'.$imagem->picture;
                $imagem->srcsetArr = [
                    '270w' => '/campanhas/'.$item->campaign_internal_code.'/montra/rev-'.$imagem->updated_at.'/'.$imagem->picture,
                    '415w' => '/campanhas/'.$item->campaign_internal_code.'/maxthumb/rev-'.$imagem->updated_at.'/'.$imagem->picture,
                    '2x'   => '/campanhas/'.$item->campaign_internal_code.'/produtos/rev-'.$imagem->updated_at.'/'.$imagem->picture,
                ];
            });

            // Check if there is available stock
            $productTotalStock = 0;
            $item->stocks->each(function ($item, $key) use (&$productTotalStock) {
                // Available stock for each size
                $item->stock_available = max(
                    0,
                    $item->stock_inicial - ($item->stock_vendido + $item->stock_aguardar_pagamento)
                );
                $productTotalStock += $item->stock_available;
            });
            // Available stock for all sizes of product
            $item->stock_available = $productTotalStock;

            $item->url = '/'.Utils::WildCardFriendly($item->campanha_nome).'/'.$this->categorySlugArray[0].'/produto/'.Utils::WildCardFriendly($item->nome).'/'.$item->internal_code;
        });

        $out = $products->toArray();

        $available = [];
        $unavailable = [];

        foreach($out as $product){
            if($product['stock_available']){
                $available[] = $product;
            } else {
                $unavailable[] = $product;
            }
        }

        return array_merge($available , $unavailable);
    }

    public function getProductsQuery()
    {
        $productsQuery = Produto::with(array('imagens' => function ($q) {
            $q->select('picture', 'id_produto', 'updated_at')->orderBy('position');
        }))
                                    ->with(array('stocks' => function ($q) {
                                        $q->select('tamanho', 'id', 'id_produto', 'preco_custo', 'preco_cf', 'preco_pvp', 'stock_inicial', 'stock_vendido', 'stock_aguardar_pagamento')
                                          ->where('stock_activo', '=', 'Y')
                                          ->whereRaw('preco_cf > preco_custo')
                                          ->whereRaw('preco_pvp >= preco_cf');
                                    }))
                                    ->with('categorias', 'marca')
                                ->select('produtos.id', 'produtos.id_campanha', 'produtos.referencia', 'produtos.informacao_tamanhos', 'produtos.mais_info', 'produtos.descricao', 'produtos.id_marca', 'produtos.nome', 'produtos.internal_code', 'C.internal_code as campaign_internal_code', 'C.nome as campanha_nome')
                                ->join('campanhas as C', 'C.id', '=', 'id_campanha')
                                ->join('produtos_stock as PS', 'PS.id_produto', '=', 'produtos.id')
                                ->join('produto_has_categoria as PC', 'produtos.id', '=', 'PC.id_produto')
                                ->leftJoin('marcas as M', 'M.id', '=', 'produtos.id_marca');

        if (false === $this->previewMode) {
            $productsQuery->where('C.data_inicio', '<=', date('Y-m-d H:i:s'))
                          ->where('C.valida', '=', 'Y')
                          ->where('C.activa', '=', 'Y')
                          ->where('C.preview', '=', 'N')
                          ->where('activo', '=', 'Y');
        }

        if ($this->priceRange['min'] >=0) {
            $productsQuery->where('PS.preco_cf', '>=', $this->priceRange['min']);
        }
        if ($this->priceRange['max'] >=0) {
            $productsQuery->where('PS.preco_cf', '<=', $this->priceRange['max']);
        }
        if (!empty($this->sizes) && count($this->sizes) > 0) {
            $productsQuery->whereIn('PS.tamanho', $this->sizes);
        }
        if (!empty($this->brandIDs) && $this->brandIDs != null) {
            $productsQuery->whereIn('produtos.id_marca', $this->brandIDs);
        }
        if (isset($this->childrenCatIDs) && !empty($this->childrenCatIDs)) {
            $productsQuery->whereIn('PC.id_categoria', $this->childrenCatIDs);
        }
        if (isset($this->campaignIDs) && !empty($this->campaignIDs)) {
            $productsQuery->whereIn('produtos.id_campanha', $this->campaignIDs);
        }
        if (isset($this->orderByCatIDs) && !empty($this->orderByCatIDs)) {
            $sqlArr = '(';
            for ($i=0; $i<count($this->orderByCatIDs); $i++) {
                $sqlArr .= $this->orderByCatIDs[$i];
                if ($i < (count($this->orderByCatIDs) -1)) {
                    $sqlArr .= ',';
                }
            }
            $sqlArr .= ')';
            $productsQuery->orderBy($this->db->eloquent()->connection()->raw('CASE WHEN (PC.id_categoria in '.$sqlArr.') THEN 1 ELSE 2 END'));
        }

        $productsQuery->where('C.data_fim', '>=', date('Y-m-d H:i:s'))
                      ->where('PS.stock_activo', '=', 'Y')
                      ->where('PS.preco_cf', '>', 0)
                      ->where('activo', '=', 'Y')
                      ->whereRaw('PS.preco_cf > PS.preco_custo')
                      ->whereRaw('PS.preco_pvp >= PS.preco_cf')
                      ->groupBy('produtos.id');
    
        foreach ($this->order as $column => $direction) {
            $productsQuery->orderBy($column, $direction);
        }

        $productsQuery->orderBy('PS.preco_cf', 'ASC');

        return $productsQuery;
    }

    public function getCampaigns()
    {
        $campaigns = Campanha::join('produtos', 'produtos.id_campanha', '=', 'campanhas.id')
                             ->join('produtos_stock', 'produtos.id', '=', 'produtos_stock.id_produto')
                             ->join('produto_has_categoria as PC', 'produtos.id', '=', 'PC.id_produto')
                             ->select('campanhas.nome', 'campanhas.internal_code', 'campanhas.id');

        if (isset($this->childrenCatIDs) && !empty($this->childrenCatIDs)) {
            $campaigns->whereIn('PC.id_categoria', $this->childrenCatIDs);
        }

        if ($this->priceRange['min'] >=0) {
            $campaigns->where('preco_cf', '>=', $this->priceRange['min']);
        }
        if ($this->priceRange['max'] >=0) {
            $campaigns->where('preco_cf', '<=', $this->priceRange['max']);
        }

        if (!empty($this->brandIDs) && $this->brandIDs != null) {
            $campaigns->whereIn('produtos.id_marca', $this->brandIDs);
        }

        if (!empty($this->sizes) && count($this->sizes) > 0) {
            $campaigns->whereIn('produtos_stock.tamanho', $this->sizes);
        }

        $result = $campaigns->where('campanhas.data_fim', '>=', date('Y-m-d H:i:s'))
                            ->where('campanhas.data_inicio', '<=', date('Y-m-d H:i:s'))
                            ->where('produtos_stock.stock_activo', '=', 'Y')
                            ->where('produtos_stock.preco_cf', '>', 0)
                            ->where('produtos_stock.preco_cf', '>', 'produtos_stock.preco_custo')
                            ->where('produtos.activo', '=', 'Y')
                            ->groupBy('campanhas.id')
                            ->get();

        return $result->toArray();
    }

    // Function to return all sizes of campaign with filters
    public function getSizes()
    {
        $sizes = Produto::join('produtos_stock', 'produtos.id', '=', 'id_produto')
                        ->join('campanhas', 'campanhas.id', '=', 'produtos.id_campanha')
                        ->join('produto_has_categoria as PC', 'produtos.id', '=', 'PC.id_produto')
                        ->select('tamanho', 'produtos_stock.id');

        if (!empty($this->campaignIDs)) {
            $sizes->whereIn('produtos.id_campanha', $this->campaignIDs);
        }

        if (isset($this->childrenCatIDs) && !empty($this->childrenCatIDs)) {
            $sizes->whereIn('PC.id_categoria', $this->childrenCatIDs);
        }

        if (!empty($this->brandIDs) && $this->brandIDs != null) {
            $sizes->whereIn('produtos.id_marca', $this->brandIDs);
        }

        if ($this->priceRange['min'] >=0) {
            $sizes->where('preco_cf', '>=', $this->priceRange['min']);
        }
        if ($this->priceRange['max'] >=0) {
            $sizes->where('preco_cf', '<=', $this->priceRange['max']);
        }

        $result = $sizes->where('campanhas.data_fim', '>=', date('Y-m-d H:i:s'))
                        ->where('produtos_stock.stock_activo', '=', 'Y')
                        ->where('produtos_stock.preco_cf', '>', 0)
                        ->where('produtos_stock.preco_cf', '>', 'produtos_stock.preco_custo')
                        ->where('produtos.activo', '=', 'Y')
                        ->groupBy('tamanho')
                        ->get();

        if ($result->isEmpty()) {
            return [];
        }
        return $result->toArray();
    }

    // Function to return all brands of campaign with filters
    public function getBrands()
    {
        $sizes = Produto::join('produtos_stock', 'produtos.id', '=', 'id_produto')
                        ->join('campanhas', 'campanhas.id', '=', 'produtos.id_campanha')
                        ->join('marcas', 'marcas.id', '=', 'produtos.id_marca')
                        ->join('produto_has_categoria as PC', 'produtos.id', '=', 'PC.id_produto')
                        ->select('marcas.id', 'marcas.nome');

        if (!empty($this->campaignIDs)) {
            $sizes->whereIn('produtos.id_campanha', $this->campaignIDs);
        }

        if (isset($this->childrenCatIDs) && !empty($this->childrenCatIDs)) {
            $sizes->whereIn('PC.id_categoria', $this->childrenCatIDs);
        }

        if (!empty($this->sizes) && count($this->sizes) > 0) {
            $sizes->whereIn('produtos_stock.tamanho', $this->sizes);
        }

        if ($this->priceRange['min'] >=0) {
            $sizes->where('preco_cf', '>=', $this->priceRange['min']);
        }
        if ($this->priceRange['max'] >=0) {
            $sizes->where('preco_cf', '<=', $this->priceRange['max']);
        }

        $result = $sizes->where('campanhas.data_fim', '>=', date('Y-m-d H:i:s'))
                        ->where('produtos_stock.stock_activo', '=', 'Y')
                        ->where('produtos_stock.preco_cf', '>', 0)
                        ->where('produtos_stock.preco_cf', '>', 'produtos_stock.preco_custo')
                        ->where('produtos.activo', '=', 'Y')
                        ->groupBy('marcas.id')
                        ->get();

        if ($result->isEmpty()) {
            return [];
        }
        return $result->toArray();
    }

    // Function to return price range of loaded campaign
    public function getPrices()
    {
        $priceRange = Produto::join('produtos_stock', 'produtos.id', '=', 'id_produto')
                             ->join('campanhas', 'campanhas.id', '=', 'produtos.id_campanha')
                             ->join('produto_has_categoria as PC', 'produtos.id', '=', 'PC.id_produto')
                             ->select($this->db->eloquent()->connection()->raw('max(preco_cf) as max, min(preco_cf) as min'));

        if (!empty($this->campaignIDs)) {
            $priceRange->whereIn('produtos.id_campanha', $this->campaignIDs);
        }
        if (isset($this->childrenCatIDs) && !empty($this->childrenCatIDs)) {
            $priceRange->whereIn('PC.id_categoria', $this->childrenCatIDs);
        }

        if (!empty($this->brandIDs) && $this->brandIDs != null) {
            $priceRange->whereIn('produtos.id_marca', $this->brandIDs);
        }

        $priceRange = $priceRange->where('campanhas.data_fim', '>=', date('Y-m-d H:i:s'))
                                 ->where('produtos_stock.stock_activo', '=', 'Y')
                                 ->where('produtos_stock.preco_cf', '>', 0)
                                 ->where('produtos_stock.preco_cf', '>', 'produtos_stock.preco_custo')
                                 ->where('produtos.activo', '=', 'Y')
                                 ->first();

        $priceRangeArr = $priceRange->toArray();

        $min = $priceRangeArr['min'];
        $max = $priceRangeArr['max'];

        $range = $max-$min;
        $interval = $range / 8;

        $prices = [0 => 0, $this->roundDownToAny($min) => $this->roundDownToAny($min)];

        for ($i=1; $i<8; $i++) {
            $prices[$this->roundToAny($min+($interval*$i))] = $this->roundToAny($min+($interval*$i));
        }
        $prices[$this->roundUpToAny($max)] = $this->roundUpToAny($max);

        return $prices;
    }

    // Function to return all categories containing products of current campaign
    public function getCatStructure()
    {
        $tree = $this->getTree();
        $treeData = $tree->data;

        $countCats = $this->countCatsQuery();
        $treeWithCount = $this->countCatStructProducts($treeData, $countCats);

        $countCatsFiltered = $this->countCatsQuery(true);
        $treeWithFilteredCount = $this->countCatStructProducts($treeWithCount, $countCatsFiltered, true);

        $catStructure = $this->handleEmptyCats($treeWithFilteredCount);

        // Organize categories so that categories we are filtering come first in tree
        if (isset($this->categorySlugArray) && !empty($this->categorySlugArray)) {
            foreach ($catStructure as $key => $item) {
                if ($item['slug'] == $this->categorySlugArray[0]) {
                    unset($catStructure[$key]);
                    array_unshift($catStructure, $item);
                }
            }
        }

        return array_values($catStructure);
    }

    private function countCatsQuery($filter = false)
    {
        $countCats = Produto::distinct()
                            ->selectRaw('PC.id_categoria, count(*) as cnt')
                            ->join('produtos_stock', 'id_produto', '=', 'produtos.id')
                            ->join('campanhas', 'campanhas.id', '=', 'produtos.id_campanha')
                            ->join('produto_has_categoria as PC', 'produtos.id', '=', 'PC.id_produto');

        if ($filter) {
            if ($this->priceRange['min'] >=0) {
                $countCats->where('produtos_stock.preco_cf', '>=', $this->priceRange['min']);
            }
            if ($this->priceRange['max'] >=0) {
                $countCats->where('produtos_stock.preco_cf', '<=', $this->priceRange['max']);
            }
            if (!empty($this->sizes) && count($this->sizes) > 0) {
                $countCats->whereIn('produtos_stock.tamanho', $this->sizes);
            }

            if (isset($this->campaignIDs) && !empty($this->campaignIDs)) {
                $countCats->whereIn('produtos.id_campanha', $this->campaignIDs);
            }

            if (!empty($this->brandIDs) && $this->brandIDs != null) {
                $countCats->whereIn('produtos.id_marca', $this->brandIDs);
            }
        }

        $result = $countCats->where('campanhas.data_fim', '>=', date('Y-m-d H:i:s'))
                            ->where('produtos_stock.stock_activo', '=', 'Y')
                            ->where('produtos_stock.preco_cf', '>', 0)
                            ->where('produtos_stock.preco_cf', '>', 'produtos_stock.preco_custo')
                            ->where('produtos.activo', '=', 'Y')
                            ->groupBy('PC.id_categoria')
                            ->get();

        return $result->toArray();
    }

    // Auxiliary function to build campaign specific category structure
    private function countCatStructProducts($tree, $countCats, $filtered = false)
    {
        $count = 'count';
        if ($filtered) {
            $count = 'countFiltered';
        }

        foreach ($tree as $key => $item) {
            $tree[$key][$count] = 0;
            foreach ($countCats as $c) {
                if ($c['id_categoria'] == $item['id']) {
                    $tree[$key][$count] = $c['cnt'];
                }
            }
            $tree[$key]['children'] = $this->countCatStructProducts($tree[$key]['children'], $countCats, $filtered);
        }
        return $tree;
    }

    // Function to unset empty Categories , checking all children categories to be sure they are empty
    private function handleEmptyCats($tree, $grayOut = false, $delete = false)
    {
        foreach ($tree as $key => $item) {
            if ($item['id_pai'] == 0) {
                $delete = false;
                $grayOut = false;
            }

            // graying out of categories present in site but with no current selection products is deactivated
            $delete = $item['count'] == 0 ? true : false;
            $delete = $item['countFiltered'] == 0 ? true : false;
            $grayOut = false;

            $tree[$key]['children'] = $this->handleEmptyCats($tree[$key]['children'], $grayOut, $delete);

            foreach ($tree[$key]['children'] as $item) {
                if ($item['gray_out'] == false) {
                    $grayOut = false;
                }
            }
            if (!empty($tree[$key]['children'])) {
                $delete = false;
            }

            $tree[$key]['gray_out'] = $grayOut;
            if ($delete) {
                unset($tree[$key]);
            }
        }
        return $tree;
    }

    // Function to return total number of products of loaded campaign
    public function countTotalProducts()
    {
        $totalProducts = Produto::join('produtos_stock', 'produtos.id', '=', 'produtos_stock.id_produto')
                                ->join('campanhas', 'campanhas.id', '=', 'produtos.id_campanha')
                                ->join('produto_has_categoria', 'produtos.id', '=', 'produto_has_categoria.id_produto');

        if (isset($this->campaignIDs) && !empty($this->campaignIDs)) {
            $totalProducts->whereIn('produtos.id_campanha', $this->campaignIDs);
        }
        if ($this->priceRange['min'] >=0) {
            $totalProducts->where('produtos_stock.preco_cf', '>=', $this->priceRange['min']);
        }
        if ($this->priceRange['max'] >0) {
            $totalProducts->where('produtos_stock.preco_cf', '<=', $this->priceRange['max']);
        }
        if (!empty($this->sizes) && count($this->sizes) > 0) {
            $totalProducts->whereIn('produtos_stock.tamanho', $this->sizes);
        }

        if (!empty($this->brandIDs) && $this->brandIDs != null) {
            $totalProducts->whereIn('produtos.id_marca', $this->brandIDs);
        }

        if (isset($this->childrenCatIDs) && !empty($this->childrenCatIDs)) {
            $totalProducts->whereIn('produto_has_categoria.id_categoria', $this->childrenCatIDs);
        }

        $result =  $totalProducts->where('campanhas.data_fim', '>=', date('Y-m-d H:i:s'))
                                 ->where('produtos_stock.stock_activo', '=', 'Y')
                                 ->where('produtos_stock.preco_cf', '>', 0)
                                 ->where('produtos_stock.preco_cf', '>', 'produtos_stock.preco_custo')
                                 ->where('produtos.activo', '=', 'Y')
                                 ->distinct()
                                 ->count(['produtos.id']);

        return $result;
    }

    // Function to determine if campaign is new
    public function isNew()
    {
        $today = new \DateTime();
        $cStarting = new \DateTime($this->data->data_inicio);
        $interval = $today->diff($cStarting);
        if (new \DateTime($this->data->data_fim) < new \DateTime()) {
            return false;
        }
        if ($interval->days != 0) {
            return false;
        }
        return true;
    }

    // Function to determine if campaign is ending
    public function isEnding()
    {
        $today = new \DateTime();
        $cEnding = new \DateTime($this->data->data_fim);
        $interval = $today->diff($cEnding);
        if ($interval->days != 0) {
            return false;
        }
        if (new \DateTime($this->data->data_inicio) > new \DateTime()) {
            return false;
        }
        return true;
    }

    // Function to determine if campaign is neither ending nor new but active
    public function isCurrent()
    {
        $today = new \DateTime();
        $cEnding = new \DateTime($this->data->data_fim);
        $endInterval = $today->diff($cEnding);

        $today = new \DateTime();
        $cStarting = new \DateTime($this->data->data_inicio);
        $startInterval = $today->diff($cStarting);

        if ($startInterval->days < 1) {
            return false;
        }
        if ($endInterval->days < 1) {
            return false;
        }
        return true;
    }

    private function isInPreview()
    {
        $c = $this->data;

        if (
            strtotime($c->data_inicio) <= time() &&
            strtotime($c->data_fim) >= time() &&
            $c->valida == 'Y' &&
            $c->activa == 'Y' &&
            $c->preview == 'N'
        ) {
            return false;
        }

        return true;
    }

    private function getTree()
    {
        if (!$this->tree) {
            $this->tree = new Tree();
        }
        return $this->tree;
    }

    // Function that rounds n to nearest multiple of x
    private static function roundToAny($n, $x=5)
    {
        return intval((round($n)%$x < ($x/2)) ? self::roundDownToAny($n, $x) : self::roundUpToAny($n, $x));
    }

    // Function that rounds n to next multiple of x
    private static function roundUpToAny($n, $x=5)
    {
        return round(($n+$x/2)/$x)*$x;
    }

    // Function that rounds n to next multiple of x
    private static function roundDownToAny($n, $x=5)
    {
        $n = floor($n);
        return $n-($n%$x);
    }
}//end class